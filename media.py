#!/usr/bin/env python3

import os, re, argparse, sys, inquirer

def cli():
    parser = argparse.ArgumentParser(
        description = 'CLI app to stream management'
    )

    

    subparser = parser.add_subparsers()

    videos = subparser.add_parser('videos',
        help = 'Action over video media resources')
    videos.add_argument('--search', type=str, help='Search videos by name')
    videos.add_argument('--preview', action="store_true", help='Show video thumb preview on kitty terminals')
    videos.set_defaults(action='videos')
    videos.add_argument('--novideo',
        action="store_true",
        help='Only sound on video resources')

    radios = subparser.add_parser('radios',
        help = 'Action over radios media resources')
    radios.add_argument('--search', type=str, help='Search radios by name')
    radios.set_defaults(action='radios')

    authors = subparser.add_parser('authors',
        help = 'Action over authors(channels on youtube) media resources'
    )
    authors.add_argument('--search', type=str, help='Search authors by name')
    authors.set_defaults(action='authors')
    authors.add_argument('--preview', action="store_true", help='Show video thumb preview on kitty terminals')
    authors.add_argument('--novideo',
        action="store_true",
        help='Only sound on video resources')

    favorites = subparser.add_parser('favorites',
        help = 'Action over favorites saved media resources'
    )
    favorites.add_argument('--list', action='store_true', help='List all saved medias')
    favorites.set_defaults(action='favorites')
    favorites.add_argument('--preview', action="store_true", help='Show video thumb preview on kitty terminals')
    favorites.add_argument('--novideo',
        action="store_true",
        help='Only sound on video resources')

    return parser

def build_url(arg, name):
    stdin = os.popen(f'sm {arg} --search "{name}"|fzf|cut -d " " -f 1')
    id = stdin.read()
    stdin = os.popen(f'sm {arg} --export-url {id}')
    return stdin.read() 

def search_videos(name):
    stdin = os.popen(f'sm videos --search "{name}"|fzf --delimiter="\t" --with-nth 3')
    row = stdin.read()
    if len(row) == 0: 
        print('Nothing selected')
        sys.exit(-1)
    id = row.split('\t')[0]
    return id

def search_videos_preview(name):
    cmd_sm = f'sm videos --download-thumbs --search "{name}"'
    cmd_fzf = '|fzf --delimiter="\t" --with-nth 3 --preview "fzf_preview_img {}"'
    stdin = os.popen(cmd_sm + cmd_fzf)
    row = stdin.read()
    if len(row) == 0: 
        print('Nothing selected')
        sys.exit(-1)
    id = row.split('\t')[0]
    return id

def play_video(id, novideo):
    stdin = os.popen(f'sm videos --export-url {id}')
    url = stdin.read()
    url = re.escape(f'{url}&quality=dash')
    novideo = novideo and '--no-video'
    os.system(f'mpv --profile=1080p --autofit=60% {url} {novideo}')

def search_radios(name):
    stdin = os.popen(f'sm radios --search "{name}"|fzf --delimiter="\t" --with-nth 2,3,4')
    row = stdin.read()
    if len(row) == 0: 
        print('Nothing selected')
        sys.exit(-1)
    id = row.split('\t')[0]
    return id

def play_radio(id):
    stdin = os.popen(f'sm radios --export-url {id}')
    url = stdin.read()
    url = re.escape(f'{url}')
    os.system(f'mpv {url} --no-video')

def search_authors(args):
    name = args.search
    stdin = os.popen(f'sm authors --search "{name}"|fzf --delimiter="\t" --with-nth 2')
    row = stdin.read()
    if len(row) == 0: 
        print('Nothing selected')
        sys.exit(-1)
    id = row.split('\t')[0]
    return id

def play_author_last_videos_preview(id, novideo):
    cmd_sm = f'sm authors --download-thumbs --last-videos "{id}"'
    cmd_fzf = '|fzf --delimiter="\t" --with-nth 3 --preview "fzf_preview_img {}"'
    stdin = os.popen(cmd_sm + cmd_fzf)
    row = stdin.read()
    if len(row) == 0: 
        print('Nothing selected')
        sys.exit(-1)
    id = row.split('\t')[0]
    stdin = os.popen(f'sm videos --export-url {id}')
    url = stdin.read()
    url = re.escape(f'{url}&quality=dash')
    novideo = novideo and '--no-video'
    os.system(f'mpv --profile=1080p --autofit=60% {url} {novideo}')

def play_author_last_videos(id, novideo):
    stdin = os.popen(f'sm authors --last-videos "{id}"|fzf --delimiter="\t" --with-nth 3')
    row = stdin.read()
    if len(row) == 0: 
        print('Nothing selected')
        sys.exit(-1)
    id = row.split('\t')[0]
    stdin = os.popen(f'sm videos --export-url {id}')
    url = stdin.read()
    url = re.escape(f'{url}&quality=dash')
    novideo = novideo and '--no-video'
    os.system(f'mpv --profile=1080p --autofit=60% {url} {novideo}')

def list_favorites():
    stdin = os.popen(f'sm favorites --list|fzf --delimiter="\t" --with-nth=2,3')
    row = stdin.read()
    if len(row) == 0: 
        print('Nothing selected')
        sys.exit(-1)
    id, type, *_ = row.split('\t')
    type = type.rstrip()
    return (id, type)

def delete_favorite(id):
    os.system(f'sm favorites --delete {id}')
    print(f'Deleted {id} from favorites')

def play_favorite(type, id, novideo, preview):
    if type == 'radio':
        stdin = os.popen(f'sm radios --get {id}|cut -d "\t" -f 1')
        id = stdin.read()
        play_radio(id)
    if type == 'author':
        stdin = os.popen(f'sm authors --get {id}|cut -d "\t" -f 4')
        id = stdin.read()
        if preview:
            play_author_last_videos_preview(id, novideo)
        else :
            play_author_last_videos(id, novideo) 

def add_video_to_favorites(id):
    os.system(f'sm favorites --add:video {id}')
    print(f'Video {id} add to favorites')

def add_author_to_favorites(id):
    os.system(f'sm favorites --add:author {id}')
    print(f'Author {id} add to favorites')
    
def ask_for_play_or_add():
    questions = [
        inquirer.List('ask',
                  message='What do you want',
                  choices=['Play', 'Add to favorites'],
                ),
    ]
    resp = inquirer.prompt(questions)
    if resp is None:
        return None
    elif 'Play' in resp['ask']:
        return 'play'
    elif 'Add' in resp['ask']:
        return 'add'

def author_ask_for():
    questions = [
        inquirer.List('ask',
                  message='What do you want',
                  choices=['List last videos', 'Add to favorites'],
                ),
    ]
    resp = inquirer.prompt(questions)
    if resp is None:
        return None
    elif 'List' in resp['ask']:
        return 'list'
    elif 'Add' in resp['ask']:
        return 'add'

def ask_for_list_or_delete():
    questions = [
        inquirer.List('ask',
                  message='What do you want',
                  choices=['List latest', 'Delete from favorites'],
                ),
    ]
    resp = inquirer.prompt(questions)
    if resp is None:
        return None 
    elif 'List' in resp['ask']:
        return 'list'
    elif 'Delete' in resp['ask']:
        return 'delete'

def ask_for_play_or_delete():
    questions = [
        inquirer.List('ask',
                  message='What do you want',
                  choices=['Play', 'Delete from favorites'],
                ),
    ]
    resp = inquirer.prompt(questions)
    if resp is None:
        return None 
    elif 'Play' in resp['ask']:
        return 'play'
    elif 'Delete' in resp['ask']:
        return 'delete'

def main():
    parser = cli()
    args = parser.parse_args()
    print(args)
    match args.action:
        case 'videos':
            if args.search: 
                if args.preview:
                    id = search_videos_preview(args.search)
                else:
                    id = search_videos(args.search)
                resp = ask_for_play_or_add()
                if resp == 'play':
                    play_video(id, args.novideo)
                elif resp == 'add':
                    add_video_to_favorites(id)
                else:
                    exit(-1)
        case 'radios':
            if args.search:
                id = search_radios(args.search)
                play_radio(id)
        case 'authors':
            if args.search: 
                id = search_authors(args)
                resp = author_ask_for()
                if resp == 'list':
                    if args.preview:
                        play_author_last_videos_preview(id, args.novideo)
                    else: 
                        play_author_last_videos(id, args.novideo)
                elif resp == 'add':
                    add_author_to_favorites(id)
                else:
                    exit(-1)
        case 'favorites':
            if args.list:
                id, type = list_favorites()
                if type == 'author':
                    resp = ask_for_list_or_delete()
                    if resp == 'list':
                        play_favorite(type, id, args.novideo, args.preview)
                    elif resp == 'delete':
                        delete_favorite(id)
                    else:
                        exit(-1)
                else:
                    resp = ask_for_play_or_delete()
                    if resp == 'play':
                        play_favorite(type, id, args.novideo, args.preview)
                    elif resp == 'delete':
                        delete_favorite(id)
                    else:
                        exit(-1)
                
            
if __name__ == '__main__':
   main()

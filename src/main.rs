use std::error::Error;
use std::process::exit;

use framework::Framework;

mod actions;
mod args;
mod ddbb;
mod framework;
mod datasrc;
mod utils;

type Result<T> = std::result::Result<T, Box<dyn Error + Send + Sync>>;

#[tokio::main]
async fn main() -> Result<()> {
    match Framework::constructor() {
        Ok(fw) => {
            fw.run().await.unwrap();
            fw.clean();
        }
        Err(err) => { 
            println!("{:?}", err);
            exit(-1);
        }
    }
    /*
    let db = Database::connect("mps.db3")?;
    db.create_tables()?;
    db.disconnect()?;
    let streamer = Streamer::new(
        "PX1",
        "PX1",
        "Artic Radio",
        "root",
        vec!["radio", "radio-garden"],
        "https://radio-garden/px1",
    );
    let remote_folder = RemoteFolder::new(
        "PX2",
        "PX2",
        "Misterio ovni",
        "root",
        vec!["channel", "yt"],
        "https://invidius-mako/px1",
    );
    let folder_two_items = Folder::new(
        "F1",
        "Misc things",
        "root",
        vec!["channel", "yt"],
        vec!["PX3", "PX5"],
    );
    let folder_child_stream = Streamer::new(
        "PX3",
        "PX3",
        "Bongo radio",
        "F1",
        vec!["radio", "radio-garden"],
        "https://radio-garden/px3",
    );
    let folder_child_remote = RemoteFolder::new(
        "PX5",
        "PX5",
        "Misterios del mas alla",
        "F1",
        vec!["channel", "yt"],
        "https://invidius-mako/px1",
    );

    let mut medias = Medias::new();
    medias.add(&streamer);
    medias.add(&remote_folder);
    medias.add(&folder_two_items);
    medias.add(&folder_child_remote);
    medias.add(&folder_child_stream);
    medias.run();
    */
    Ok(())
}

#!/usr/bin/env python3

import json, sqlite3, sys, uuid

file = open('radios.json')
radios = json.load(file)
if not radios:
    print('Error al leer los radios, el fichero esta vacio')
    sys.exit(2)
conn = sqlite3.connect("./sm.db3")
cur = conn.cursor()
for radio in radios:
    print(f'Inseting {radio["title"]}')
    fav_id = uuid.uuid4()
    radio_id = uuid.uuid4()
    cur.execute('insert into favorites values (?, ?, ?, ?)',
                 (
                     str(fav_id),
                     'radio',
                     radio['title'],
                     f'https://radio.garden/api/ara/content/listen/{radio["id"]}/channel.mp3',
                 ),
                 )
    conn.commit()
    cur.execute('insert into radios values (?, ?, ?, ?, ?, ?)',
                 (
                      str(radio_id),
                      str(fav_id),
                      radio['title'],
                      radio['id'],
                      radio['place']['title'],
                      radio['country']['title'],
                  ),
                )
    conn.commit()
cur.close()

use std::error;
use std::path::PathBuf;
use dirs::cache_dir;
use uuid::Uuid;
use image::{save_buffer, ColorType};

use crate::datasrc::db;
use crate::datasrc::req;
use crate::ddbb::Database;
use crate::utils::build_cache_dir;

async fn resolve_instance(db: &Database, nocache: bool)
    -> Result<String, Box<dyn error::Error>> {
    let exist_instances = db::Instance::exists(db)?;
    if nocache || !exist_instances {
        let inv_instance = req::Instance::get().await?;
        if exist_instances { 
            db::Instance::update(db, &inv_instance, "top")?;
        } else {
            db::Instance::insert(db, &inv_instance, "top")?;
        }
        Ok(inv_instance)
    } else {
        Ok(db::Instance::select(db)?)
    }
}

pub async fn search_videos_with_images(
    db: &Database,
    name: &str,
    nocache: bool,
) -> Result<(), Box<dyn error::Error>> {
    let inv_instance = resolve_instance(db, nocache).await?;   
    let videos = req::Video::get(name, &inv_instance).await?;
    let mut spawns = vec![];
    for video in videos {
        let thumb_option = video.video_thumbnails.into_iter().find(|thumb| thumb.quality == "medium");
        if let Some(thumb) = thumb_option {
            let spawn = tokio::task::spawn(async move { 
                let attach_bytes_result = req::Video::req_get_thumb(thumb.url).await;
                if let Ok(attach_bytes) = attach_bytes_result {
                    let image_result = image::load_from_memory(&attach_bytes);
                    if let Ok(image) = image_result {
                        let mut cache_file = build_cache_dir("sm").unwrap();
                        cache_file.push(format!("{}.jpg", video.video_id));
                        if image.save(cache_file).is_ok() {
                            println!("{}\t{}\t{}", video.video_id, video.author_id, video.title);
                        } else {
                            println!("[SPAWN][IMG][SAVE][ERROR] {}", video.video_id);
                        }
                    }
                }
            });
            spawns.push(spawn);
        } 
    }
    for spawn in spawns {
        spawn.await.unwrap();
    }

    Ok(())
}

pub async fn search_videos(
    db: &Database,
    name: &str,
    nocache: bool,
) -> Result<(), Box<dyn error::Error>> {
    let inv_instance = resolve_instance(db, nocache).await?;   
    let videos = req::Video::get(name, &inv_instance).await?;
    println!("{} videos found", videos.len());
    req::Video::print(&videos);

    Ok(())
}

pub async fn export_video(
    db: &Database,
    id: &str,
    nocache: bool,
) -> Result<(), Box<dyn error::Error>> {
    let inv_instance = resolve_instance(db, nocache).await?;
    println!("https://{}/watch?v={}", inv_instance, id);
    Ok(())
}

pub async fn search_authors(
    db: &Database,
    name: &str,
    nocache: bool,
) -> Result<(), Box<dyn error::Error>> {
    let inv_instance = resolve_instance(db, nocache).await?;   
    let authors = req::Author::get(name, &inv_instance).await?;
    println!("{} authors found", authors.len());
    req::Author::print(&authors); 

    Ok(())
}

pub async fn search_radios(
    name: &str,
) -> Result<(), Box<dyn error::Error>> {
    let radios = req::Radio::get(name).await?;
    println!("{} radios found", radios.len());
    req::Radio::print(&radios);

    Ok(())
}

pub fn export_radio(
    id: &str,
) -> Result<(), Box<dyn error::Error>> {
    println!("https://radio.garden/api/ara/content/listen/{}/channel.mp3", id);
    Ok(())
}

pub fn list(db: &Database)
    -> Result<(), Box<dyn error::Error>> {
    let favorites = db::Favorite::select(db)?;
    db::Favorite::print(favorites);
    Ok(())
}

pub fn get_fav_radio(db: &Database, id: &str)
    -> Result<(), Box<dyn error::Error>> {
    let radio = db::Favorite::select_radios_row(db, id)?;
    db::Radio::print_row(&radio);
    Ok(())
}

pub fn get_fav_author(db: &Database, id: &str)
    -> Result<(), Box<dyn error::Error>> {
    let author = db::Favorite::select_authors_row(db, id)?;
    db::Author::print_row(&author);
    Ok(())
}

pub fn delete(db: &Database, id: &str)
    -> Result<(), Box<dyn error::Error>> {
        db::Favorite::delete(db, id)?;
        Ok(())
}

pub async fn get_latest(db: &Database, id: &str, nocache: bool)
    -> Result<(), Box<dyn error::Error>> {
        let inv_instance = resolve_instance(db, nocache).await?;   
        let videos = req::Author::get_latest(id, &inv_instance).await?;
        req::Video::print(&videos);

        Ok(())
}

pub async fn get_latest_with_images(db: &Database, id: &str, nocache: bool)
    -> Result<(), Box<dyn error::Error>> {
        let inv_instance = resolve_instance(db, nocache).await?;   
        let videos = req::Author::get_latest(id, &inv_instance).await?;
        let mut spawns = vec![];
        for video in videos {
            let thumb_option = video.video_thumbnails.into_iter().find(|thumb| thumb.quality == "medium");
            if let Some(thumb) = thumb_option {
                let spawn = tokio::task::spawn(async move { 
                    let attach_bytes_result = req::Video::req_get_thumb(thumb.url).await;
                    if let Ok(attach_bytes) = attach_bytes_result {
                        let image_result = image::load_from_memory(&attach_bytes);
                        if let Ok(image) = image_result {
                            let mut cache_file = build_cache_dir("sm").unwrap();
                            cache_file.push(format!("{}.jpg", video.video_id));
                            if image.save(cache_file).is_ok() {
                                println!("{}\t{}\t{}", video.video_id, video.author_id, video.title);
                            } else {
                                println!("[SPAWN][IMG][SAVE][ERROR] {}", video.video_id);
                            }
                        }
                    }
                });
                spawns.push(spawn);
            } 
        }
        for spawn in spawns {
            spawn.await.unwrap();
        }

        Ok(())
}

pub async fn add_author(db: &Database, id: &str, nocache: bool)
    -> Result<(), Box<dyn error::Error>> {
    let inv_instance = resolve_instance(db, nocache).await?;   
    let author_api = req::Author::get_row(id, &inv_instance).await?;
    let fav_id = Uuid::new_v4().to_string();
    let author_id = Uuid::new_v4().to_string();
    let author_db = db::Author {
        id: author_id,
        fav_id: fav_id.clone(),
        name: author_api.name.clone(),
        author_id: author_api.id.clone(),
    };
    let favorite = db::Favorite {
        id: fav_id,
        fav_type: "author".to_string(),
        label: author_api.name,
        url: author_api.author_url,
    };
    db::Favorite::insert(&db, &favorite)?;
    db::Author::insert(&db, &author_db)?;

    Ok(())
}

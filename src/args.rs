use clap::{arg, command, ArgMatches, Arg};

pub fn parse() -> ArgMatches {
    command!()
        .version("1.0.0")
        .about("Streams Manager ;)")
        .author("Dokoto <trabajo@dkt-mlk.es>")
        .arg(
            arg!(<MODE>)
                .help("Media type to use")
                .takes_value(true)
                .possible_value("videos")
                .possible_value("authors")
                .possible_value("radios")
                .possible_value("favorites")
        )

        .arg(
            Arg::new("export")
                .help("Export url")
                .long_help("$> mpv [videos|authors|radios] --export-url [id]")
                .long("export-url")
                .takes_value(true)
        )

        .arg(
            Arg::new("last")
                .help("Last videos of author by id")
                .long_help("$> mpv authors --last-videos [id]")
                .long("last-videos")
                .takes_value(true)
        )

        .arg(
            Arg::new("delete")
                .help("Delete favorites")
                .long_help("$> mpv favorites --delete [id]")
                .long("delete")
                .takes_value(true)
        )

        .arg(
            Arg::new("search")
                .help("Search by name")
                .long_help("$> sm [videos|authors|radios] --search [name]")
                .long("search")
                .takes_value(true),
        )

        .arg(
            Arg::new("get")
                .help("Get media favorite")
                .long_help("$> sm [videos|authors|radios] --get [id]")
                .long("get")
                .takes_value(true),
        )

        .arg(
            Arg::new("list")
                .long("list")
                .help("List media favorites")
                .long_help("$> sm favorites --list")
        )

        .arg(
            Arg::new("add_author")
                .long("add:author")
                .help("Add author(youtube channel) media to favorites")
                .long_help("$> sm favorites --add:author [id]")
                .takes_value(true),
        )

        .arg(
            Arg::new("add_radio")
                .long("add:radio")
                .help("Add radios media to favorites")
                .long_help("$> sm favorites --add:radio [id]")
                .takes_value(true),
        )

        .arg(
            Arg::new("add_video")
                .long("add:video")
                .help("Add video media to favorites")
                .long_help("$> sm favorites --add:video [id]")
                .takes_value(true),
        )

        .arg(
            Arg::new("nocache")
                .long("nocache")
                .help("Force request invidius instance")
        )

        .arg(
            Arg::new("download")
                .long("download-thumbs")
                .help("Download video thumbs")
        )
        .get_matches()
}
